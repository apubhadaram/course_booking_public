const courseForm = document.querySelector(`#courseForm`)

courseForm.addEventListener(`submit`, (e) => {
    e.preventDefault()

    inputs = courseForm.getElementsByTagName(`input`)
    labels = [`name`, `price`, `description`]

    const course = {}
    try {
        labels.forEach((label, i) => {
            if (!inputs[i].value) throw { message: `Please input course ${label}` }
            course[label] = inputs[i].value
        })

        course[`price`] = +course[`price`]
        createCourse(course)
    } catch (err) {
        Swal.fire({
            icon: `error`,
            text: err.message
        })
    }
})

const createCourse = async (course) => {
    try {
        let response = await fetch(`http://localhost:3000/api/courses/create`, {
            method: `POST`,
            headers: {
                'Content-Type': `application/json`,
                'Authorization': sessionStorage.getItem(`jwt`)
            },
            body: JSON.stringify(course)
        })

        console.log(response.status);
        let result = await response.json();

        console.log(result);

        if (response.status === 201) {
            Swal.fire({
                icon: `success`,
                text: `Course Creation Successful`
            });
        } else {
            Swal.fire({
                icon: `error`,
                text: `Course Creation Error`
            });
        }
    } catch (err) {
        console.error(err);
    }
}