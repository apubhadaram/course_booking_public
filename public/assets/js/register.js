const regForm = document.querySelector(`#registerUser`);
const eyes = document.querySelectorAll(`.fa-eye-slash`);
const passInpts = document.querySelectorAll(`input[type='password']`);

let showPass = false;

eyes.forEach((eye, i) => {
  eye.addEventListener(`click`, (e) => {
    if (!showPass) {
      passInpts[i].setAttribute(`type`, `text`);
      eyes[i].style.color = "#333333";
      showPass = true;
    } else {
      passInpts[i].setAttribute(`type`, `password`);
      eyes[i].style.color = "#999999";
      showPass = false;
    }
  });
});

passInpts.forEach((inpt, i) => {
  inpt.addEventListener(`keyup`, () => {
    if (inpt.value) {
      eyes[i].style.color = "#999999";
      eyes[i].style.cursor = `pointer`;
    } else {
      eyes[i].style.color = "white";
      eyes[i].style.cursor = `default`;
    }
  });
});

regForm.addEventListener(`submit`, (e) => {
  e.preventDefault();

  let inputs = regForm.getElementsByTagName(`input`);
  let labels = [
    `firstName`,
    `lastName`,
    `email`,
    `mobileNo`,
    `password`,
    `verifyPassword`,
  ];

  let user = {};

  let register = true;

  for (let i in labels) {
    if (!inputs[i].value) {
      Swal.fire({
        icon: `error`,
        title: `Unfilled Filled`,
        text: `Please fill all the necessary fields`,
      });
      register = false;
    }
    user[labels[i]] = inputs[i].value;
  }

  // Password: 8-15 chars and 1 special char
  let reg = /^(?=.{8,15})(?=.*[\w])(?=.*[^\w]).*$/gi;
  if (reg.test(user.password) && register) {
    registerUser(user);
  } else {
    Swal.fire({
      icon: `error`,
      title: `Invalid Password`,
      text: `Please make sure the your password has at least 1 special character and at least 8 characters up to 15 characters.`,
    });
  }
});

const registerUser = async (user) => {
  // mobileNo should accept both Foreign and Local mobile Number formats.
  if (
    user[`password`] === user[`verifyPassword`] &&
    user[`mobileNo`].length === 11
  ) {
    try {
      const response = await fetch(`http://localhost:3000/api/users/register`, {
        method: `POST`,
        headers: {
          "Content-Type": `application/json`,
        },
        body: JSON.stringify(user),
      });

      console.log(response.body);

      response.status < 300
        ? Swal.fire({
            icon: `success`,
            text: `Registered Successfully`,
          })
        : Swal.fire({
            icon: `error`,
            title: `The Email Already Exists!`,
            text: `Please use another email.`,
          });
    } catch (err) {
      console.error(err);
      Swal.fire(err.message);
    }
  } else {
    Swal.fire({
      icon: `error`,
      title: `Invalid Password`,
      text: `Please make sure your password are the same.`,
    });
  }
};
