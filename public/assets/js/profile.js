const token = sessionStorage.getItem("jwt");

console.log(token);

const getData = async () => {
  const detailsRes = await fetch("http://localhost:3000/api/users/details", {
    headers: {
      "Content-type": `application/json`,
      Authorization: token,
    },
  });

  const user = await detailsRes.json();

  let profile = document.querySelector(`#profileContainer`);

  profile.innerHTML = `
        <div class="col-md-13">
            <section class="jumbotron my-5">
                <h3 class="text-center">First Name: ${user.firstName}</h3>
                <h3 class="text-center">Last Name: ${user.lastName}</h3>
                <h3 class="text-center">Email: ${user.email}</h3>
                <h3 class="text-center">Mobile Number: ${user.mobileNo}</h3>
                <table class="table">
                    <tr>
                        <th>Course ID</th>
                        <th>Enrolled On</th>
                        <th>Status</th>
                        <tbody> </tbody>
                    </tr>
                    <tr>
                        
                    </tr>
                </table>
            </section>
        </div>
    `;
};

getData();
