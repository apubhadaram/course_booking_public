const logForm = document.querySelector("#loginForm");

const authUser = (e) => {
  e.preventDefault();

  // Assigning all the inputs to the user object and verifies whether
  const inputs = logForm.getElementsByTagName(`input`);
  const labels = [`email`, `password`];
  const user = {};

  // const user = new Map(_.zip(labels, _.map(inputs, i => i.value)))

  try {
    for (let i in labels) {
      if (!inputs[i].value)
        throw {
          title: `Empty Field`,
          text: `Please fill all the necessary fields`,
        };
      user[labels[i]] = inputs[i].value;
    }
    loginUser(user);
  } catch (err) {
    // if (err instanceof AuthenticationError) {
    //     Swal.fire({
    //         icon: `error`,
    //         title: err.title,
    //         text: err.text
    //     })
    // } else
    if (err instanceof SyntaxError) {
      Swal.fire({
        icon: `error`,
        title: err.title,
        text: err.text,
      });
    } else {
      Swal.fire({
        icon: `error`,
        title: err.title,
        text: err.text,
      });
    }
  }
};

const loginUser = async (user) => {
  try {
    // mem.setRefreshTokenEndpoint(`http://localhost:3000/api/users/refresh-token`)
    const loginRes = await fetch(`http://localhost:3000/api/users/login`, {
      method: `POST`,
      headers: {
        "Content-Type": `application/json`,
      },
      body: JSON.stringify(user),
    });

    // If response is valid
    if (loginRes.status <= 302) {
      // Getting and setting jwt token
      const { accessToken: token } = await loginRes.json();

      window.sessionStorage.setItem(`jwt`, `Bearer ${token}`);

      const detailsRes = await fetch(
        "http://localhost:3000/api/users/details",
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );

      const userDetails = await detailsRes.json();
      sessionStorage.setItem(`id`, userDetails._id);
      sessionStorage.setItem(`isAdmin`, userDetails.isAdmin);

      // Redirection
      location.href = "http://localhost:5501/public/profile.html";
    } else {
      Swal.fire({
        icon: `error`,
        title: `Login Error`,
        text: `Please make sure you have the right credentials.`,
      });
    }
  } catch (err) {
    console.log(err);
    Swal.fire({
      icon: "error",
      title: "Error!",
      text: "Something went wrong :(",
    });
  }
};

logForm.addEventListener("submit", authUser);
